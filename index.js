const {Storage} = require('@google-cloud/storage');
const storage = new Storage();


exports.upload = async (req,res) =>{
    const body = req.body;
    console.log(body)
    console.log(body.extension)
    const bucket = storage.bucket('my-first-bucket-pierce');
    const buffer = Buffer.from(body.content,'base64');
    const file = bucket.file(`${body.name}.${body.extension}`)
    await file.save(buffer);
    await file.makePublic();
    res.send({photoLink:file.publicUrl()})
};